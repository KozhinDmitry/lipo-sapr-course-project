﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using CustomStack;

namespace CollectionViewer
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Constants

        private const int MIN_RANDOM_VALUE = 1;
        private const int MAX_RANDOM_VALUE = 30;

        #endregion

        #region Fields

        private readonly Core.CollectionViewer _collectionViewer;
        private readonly Random _random;

        private CustomCollection.Collection<int> _collection;
        private CustomStack<int> _customStack;
        private object _selectedItem;
        private int _collectionElementsCount;
        private ObservableCollection<int> _collectionOperationResult;

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public MainWindowViewModel()
        {
            GenerateCollectionCommand = new DelegateCommand(OnGenerateCollection);
            GenerateStackCommand = new DelegateCommand(OnGenerateStack);
            UnionCommand = new DelegateCommand(o => OnCollectionCommand(_collectionViewer.GetUnion));
            IntersectionCommand = new DelegateCommand(o => OnCollectionCommand(_collectionViewer.GetIntersection));
            ConcatenateCommand = new DelegateCommand(o => OnCollectionCommand((ints, enumerable) =>
                _collectionViewer.Concatenate(ints, enumerable)));

            _collectionViewer = new Core.CollectionViewer();
            _random = new Random();
            CollectionElementsCount = 50;

            _customStack = _collectionViewer.CreateStack<int>();
            _collection = _collectionViewer.CreateColletion<int>();
            CollectionOperationResult = new ObservableCollection<int>();
        }

        #endregion

        #region Properties

        public ICommand GenerateCollectionCommand { get; }
        public ICommand GenerateStackCommand { get; }
        public ICommand UnionCommand { get; }
        public ICommand IntersectionCommand { get; }
        public ICommand ConcatenateCommand { get; }

        public CustomStack<int> Stack
        {
            get => _customStack;
            set
            {
                _customStack = value;
                OnPropertyChanged(nameof(Stack));
            }
        }

        public CustomCollection.Collection<int> Collection
        {
            get => _collection;
            set
            {
                _collection = value;
                OnPropertyChanged(nameof(Collection));
            }
        }

        public int CollectionElementsCount
        {
            get => _collectionElementsCount;
            set
            {
                _collectionElementsCount = value;
                OnPropertyChanged(nameof(CollectionElementsCount));
            }
        }

        public ObservableCollection<int> CollectionOperationResult
        {
            get => _collectionOperationResult;
            set
            {
                _collectionOperationResult = value;
                OnPropertyChanged(nameof(CollectionOperationResult));
            }
        }

        #endregion

        #region Methods

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnGenerateCollection(object obj)
        {
            IEnumerable<int> randomCollection = _collectionViewer.GetRandomCollection();
            foreach (var item in randomCollection)
            {
                Collection.Add(item);
            }
        }

        private void OnGenerateStack(object obj)
        {
            for (int i = 0; i < CollectionElementsCount; i++)
            {
                Stack.Push(_random.Next(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE));
            }
        }

        private void OnCollectionCommand(Func<IEnumerable<int>, IEnumerable<int>, IEnumerable<int>> handler)
        {
            CollectionOperationResult.Clear();
            var result = handler(Collection, Stack);

            foreach (var i in result)
            {
                CollectionOperationResult.Add(i);
            }
        }

        #endregion
    }
}